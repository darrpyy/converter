import unittest

from main import ConvertingService


class TestService(unittest.TestCase):
    def test_binary_to_decimal(self):
        binary_number = "101"
        decimal_number = str(int(binary_number, 2))

        self.assertEqual(
            ConvertingService.convert_binary_to_decimal(binary_number),
            decimal_number
        )

    def test_decimal_to_binary(self):
        decimal_number = "7"
        binary_number = "111"

        self.assertEqual(
            ConvertingService.convert_decimal_to_binary(decimal_number),
            binary_number
        )
