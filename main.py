from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class NumberResponse(BaseModel):
    decimal_number: str
    binary_number: str


class ConvertingService:
    @staticmethod
    def convert_binary_to_decimal(binary_number: str):
        return str(int(binary_number, 2))

    @staticmethod
    def convert_decimal_to_binary(decimal_number: str):
        return str(bin(int(decimal_number)))[2:]


@app.get("/binary/{binary_number}", response_model=NumberResponse)
async def binary_to_decimal(binary_number: str):
    decimal_number = ConvertingService.convert_binary_to_decimal(binary_number)
    return NumberResponse(
        decimal_number=str(decimal_number),
        binary_number=str(binary_number)
    )


@app.get("/decimal/{decimal_number}", response_model=NumberResponse)
async def decimal_to_binary(decimal_number: str):
    binary_number = ConvertingService.convert_decimal_to_binary(decimal_number)
    return NumberResponse(
        decimal_number=str(decimal_number),
        binary_number=str(binary_number)
    )
